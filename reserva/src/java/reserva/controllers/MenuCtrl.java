/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Menu;

/**
 *
 * @author Cleiser
 */
public class MenuCtrl {
     public ArrayList listado() {
        ArrayList lista = new ArrayList();

        Menu men1 = new Menu();
        men1.setCodigo("Inicio");
        men1.setNombre("Inicio");
        men1.setIcono("oi oi-home");
        men1.setUrl("index.jsp");
        lista.add(men1);
        
        Menu men4 = new Menu();
        men4.setCodigo("Personas");
        men4.setNombre("Personas");
        men4.setIcono("oi oi-people");
        men4.setUrl("persona/");
        lista.add(men4);
        
        Menu men3 = new Menu();
        men3.setCodigo("Producto");
        men3.setNombre("Rice Menus");
        men3.setIcono("oi oi-spreadsheet");
        men3.setUrl("producto/");
        lista.add(men3);
        
        Menu men2 = new Menu();
        men2.setCodigo("Venta");
        men2.setNombre("Venta");
        men2.setIcono("oi oi-book");
        men2.setUrl("ventas/");
        lista.add(men2);
        
        Menu men8 = new Menu();
        men8.setCodigo("Reportes");
        men8.setNombre("Reportes");
        men8.setIcono("oi oi-pie-chart");
        men8.setUrl("reportes/");
        lista.add(men8);
        
        
        Menu men5 = new Menu();
        men5.setCodigo("TipoComprobante");
        men5.setNombre("Tipo Comprobante");
        men5.setIcono("oi oi-spreadsheet");
        men5.setUrl("tipocomprobante/");
        lista.add(men5);
        
        Menu men6 = new Menu();
        men6.setCodigo("TipoProducto");
        men6.setNombre("Categoria");
        men6.setIcono("oi oi-people");
        men6.setUrl("tipo_producto/");
        lista.add(men6);
        
        Menu men7 = new Menu();
        men7.setCodigo("DetalleVenta");
        men7.setNombre("Detalle Venta");
        men7.setIcono("oi oi-graph");
        men7.setUrl("detalle_venta/");
        lista.add(men7);
        
        

        return lista;
    }
    
}
