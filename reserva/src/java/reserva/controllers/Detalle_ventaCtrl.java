/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Detalle_venta;

/**
 *
 * @author Cleiser
 */
public class Detalle_ventaCtrl {
     public ArrayList listado(String buscar) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM detalle_venta" );
        while (cx.getNext()) {
            //id,producto_id,venta_id,precio,cantidad;
            Detalle_venta dv = new Detalle_venta();
            dv.setId(cx.getCol("id"));
            dv.setProducto_id (cx.getCol("producto_id"));
            dv.setVenta_id(cx.getCol("venta_id"));
            dv.setPrecio(cx.getCol("precio"));        
            dv.setCantidad(cx.getCol("cantidad"));          
           
            lista.add(dv);
        }
        return lista;
    
}
     
      public boolean add(Detalle_venta dv ){
        Conexion cx = Configuracion.reserva();
        String com = "insert into detalle_venta(producto_id,venta_id,precio,cantidad) "
                + " values('"+dv.getProducto_id()+"','"+dv.getVenta_id()+"',"+dv.getPrecio()+","+dv.getCantidad()+")";
        //System.out.println("VALOR: "+com);
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }
        catch(Exception e){
            cx.RollBack();
            return false;
        }
    }
      
      public Detalle_venta edit(String id){
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM detalle_venta WHERE id="+id);
        cx.getNext();
          Detalle_venta dv = new Detalle_venta();
            dv.setId(cx.getCol("id"));
            dv.setProducto_id(cx.getCol("producto_id"));
            dv.setVenta_id(cx.getCol("venta_id"));
            dv.setPrecio(cx.getCol("precio"));
            dv.setCantidad(cx.getCol("cantidad"));
            
            
            return dv;
    }
        public boolean update(Detalle_venta dv){
        Conexion cx = Configuracion.reserva();
        String com = "update detalle_venta set producto_id='"+dv.getProducto_id()+"', "
                + " venta_id='"+dv.getVenta_id()+"', "
                + " precio="+dv.getPrecio()+", "
                + " cantidad='"+dv.getCantidad()+"' "
                + " WHERE id="+dv.getId();
        //System.out.println("VALOR: "+com);//imprimir error
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
        
         public boolean delete(String id){
        Conexion cx = Configuracion.reserva();
        String com = "delete from detalle_venta WHERE id="+id;
        //System.out.println("VALOR: "+com);
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
      
}
