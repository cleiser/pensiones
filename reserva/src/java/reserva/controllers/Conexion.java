package reserva.controllers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {

    private static Connection cn;
    private static Statement st;
    private static ResultSet rs;
    private static String MError;
    private static CallableStatement cst;
    private static Conexion INSTANCE;

    public Conexion() {
        this.mConection("Postgresql", "localhost", "5432", "postgres", "reserva", "123");
    }

    ;
    
    private static synchronized void CreateInstance() {
        if (Conexion.INSTANCE == null) {
            Conexion.INSTANCE = new Conexion();
        }
    }
    
    public static Conexion getInstance() {
        if (Conexion.INSTANCE == null) {
            CreateInstance();
            System.out.println("Se creo nueva instancia");
        } else {
            System.out.println("Ya existe instancia");
        }
        return Conexion.INSTANCE;
    }
    
    public void execQuery(final String com) {
        try {
            Conexion.rs = Conexion.st.executeQuery(com);
            System.out.println(com);
            Conexion.MError = "";
        } catch (Exception e) {
            // Eliminar conexion
            deleteInstance();
            Conexion.MError = e.getMessage();
            System.out.println("No se pudo ejecutar: " + Conexion.MError);
        }
    }
    
    public int execC(final String com) throws SQLException {
        final int rss = Conexion.st.executeUpdate(com);
        return rss;
    }
    
    public boolean getNext() {
        boolean valor = false;
        try {
            valor = Conexion.rs.next();
            Conexion.MError = "";
        } catch (Exception e) {
            Conexion.MError = e.getMessage();
        }
        return valor;
    }
    
    public String getCol(final String ncol) {
        String valor = new String();
        try {
            valor = Conexion.rs.getString(ncol);
            if (valor == null) {
                valor = "";
            }
            Conexion.MError = "";
        } catch (Exception e) {
            Conexion.MError = e.getMessage();
        }
        return valor;
    }
    
    public void Commit() {
        try {
            Conexion.cn.commit();
        } catch (Exception e) {
            this.RollBack();
            Conexion.MError = e.getMessage();
        }
    }
    
    public void RollBack() {
        try {
            Conexion.cn.rollback();
        } catch (Exception e) {
            Conexion.MError = e.getMessage();
        }
    }

    
    private void mConection(
            final String GestorBD,
            final String host,
            final String puerto,
            final String usuario,
            final String nombreBD,
            final String clave
    ) {
        try {
            if (GestorBD.equals("Postgresql")) {
                Class.forName("org.postgresql.Driver");
                Conexion.cn = DriverManager.getConnection("jdbc:postgresql://" + host + ":"+puerto+"/" + nombreBD + "/", usuario, clave);
                Conexion.MError = "Conectado a Postgresql correctamente";
            }
            Conexion.cn.setAutoCommit(false);
            Conexion.st = Conexion.cn.createStatement();
            Conexion.MError = "";
        } catch (Exception e) {
            Conexion.MError = e.getMessage();
        }
    }

    public static void deleteInstance() {
        Conexion.INSTANCE = null;
        try {
            Conexion.cn = null;
            Conexion.st = null;
            Conexion.rs = null;
        }
        catch(Exception e){
            Conexion.MError = e.getMessage();
        }

    }

    static {
        Conexion.cn = null;
        Conexion.st = null;
        Conexion.rs = null;
        Conexion.MError = new String();
        Conexion.cst = null;
        Conexion.INSTANCE = null;
    }
}
