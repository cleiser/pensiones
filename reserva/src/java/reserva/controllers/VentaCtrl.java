/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Venta;

/**
 *
 * @author Cleiser
 */
public class VentaCtrl {
    public ArrayList listado(String buscar) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM Venta WHERE UPPER(tipo_comprobante_id || correlativo) like UPPER('%"+buscar+"%') order by tipo_comprobante_id, correlativo" );
        while (cx.getNext()) {
            //id,tipo_comprobante_id,correlativo,cliente_id,fecha,total,vendedor_id,estado
            Venta ven = new Venta();
            ven.setId(cx.getCol("id"));
            ven.setTipo_comprobante_id(cx.getCol("tipo_comprobante_id"));
            ven.setCorrelativo(cx.getCol("correlativo"));
            ven.setCliente_id(cx.getCol("cliente_id"));
            ven.setFecha(cx.getCol("fecha"));
            ven.setTotal(cx.getCol("total"));
            ven.setVendedor_id (cx.getCol("vendedor_id"));
            ven.setEstado(cx.getCol("estado"));
            lista.add(ven);
        }
        return lista;
    }
    public boolean add(Venta ven){
        Conexion cx = Configuracion.reserva();
        String com = "insert into curso(tipo_comprobante_id,correlativo,cliente_id,fecha,total,vendedor_id,estado) "
                + " values('"+ven.getTipo_comprobante_id()+"','"+ven.getCorrelativo()+"','"+ven.getCliente_id()+"','"+ven.getFecha()+"','"+ven.getTotal()+"',"+ven.getVendedor_id()+",'"+ven.getEstado()+"')";
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }
        catch(Exception e){
            cx.RollBack();
            return false;
        }
    }
     public Venta edit(String id){
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM venta WHERE id="+id);
        cx.getNext();
          Venta ven = new Venta();
            ven.setId(cx.getCol("id"));
            ven.setTipo_comprobante_id(cx.getCol("tipo_comprobante_id"));
            ven.setCorrelativo(cx.getCol("correlativo"));
            ven.setCliente_id(cx.getCol("cliente_id"));
            ven.setFecha(cx.getCol("fecha"));
            ven.setTotal(cx.getCol("total"));
            ven.setVendedor_id (cx.getCol("vendedor_id"));
            ven.setEstado(cx.getCol("estado"));
            
            return ven;
    }
     public boolean update(Venta ven){
        Conexion cx = Configuracion.reserva();
        String com = "update venta set correlativo='"+ven.getCorrelativo()+"', "
                + " tipo_comprobante_id='"+ven.getTipo_comprobante_id()+"', "
                + " correlativo="+ven.getCorrelativo()+", "
                + " cliente_id="+ven.getCliente_id()+", "
                + " fecha="+ven.getFecha()+", "
                + " total="+ven.getTotal()+", "
                + " vendedor_id="+ven.getVendedor_id()+", "
                + " estado='"+ven.getEstado()+"' "
                + " WHERE id="+ven.getId();
        //System.out.println("VALOR: "+com);//imprimir error
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
     public boolean delete(String id ){
        Conexion cx = Configuracion.reserva();
        String com = "delete from venta WHERE id="+id;
        System.out.println("VALOR: "+com);
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
    
}
