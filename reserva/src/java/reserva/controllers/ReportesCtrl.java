/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Reportes;

/**
 *
 * @author Jared
 */
public class ReportesCtrl {

    public ArrayList listado(String buscar) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("select per.nombres as persona, tm.nombre as movimiento, sum(mov.monto) as total "
                + " from movimientos mov, tipo_movimientos tm, persona per "
                + " where tm.id = mov.id_tipo_mov and mov.id_persona = per.id  and"
                + " UPPER(per.nombres||tm.nombre) like UPPER('%" + buscar + "%') "
                + " group by per.nombres, tm.nombre  order by per.nombres  "
                + " ");
        while (cx.getNext()) {
            Reportes rep = new Reportes();
            rep.setPersona(cx.getCol("persona"));
            rep.setMovimiento(cx.getCol("movimiento"));
            rep.setTotal(cx.getCol("total"));
            lista.add(rep);
        }
        return lista;
    }

}
