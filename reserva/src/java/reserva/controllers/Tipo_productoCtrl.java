/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Tipo_producto;

/**
 *
 * @author Cleiser
 */
public class Tipo_productoCtrl {
     public ArrayList listado(String id) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM tipo_producto ");
        while (cx.getNext()) {
            //id,nombres,apellidos,dni,direccion,sexo;
            Tipo_producto tp = new Tipo_producto();
            tp.setId(cx.getCol("id"));
            tp.setNombre(cx.getCol("nombre"));
           
            lista.add(tp);
        }
        return lista;
    }
    
}
