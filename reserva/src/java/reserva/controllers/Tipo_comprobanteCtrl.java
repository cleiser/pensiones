/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Tipo_comprobante;


/**
 *
 * @author Cleiser
 */
public class Tipo_comprobanteCtrl {
    public ArrayList listado(String id) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM tipo_comprobante ");
        while (cx.getNext()) {
            //id,nombres,apellidos,dni,direccion,sexo;
            Tipo_comprobante tc = new Tipo_comprobante();
            tc.setId(cx.getCol("id"));
            tc.setNombre(cx.getCol("nombre"));
           
            lista.add(tc);
        }
        return lista;
    }
    
    
}
