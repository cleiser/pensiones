
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="icon" href="../recursos/img/biblia_.png" sizes="32x32" type="image/png">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <link href="../recursos/css/style.css" rel="stylesheet" type="text/css"/>
        <title>PRINCIPAL | CURSOS B�BLICOS</title>
        <%! String pagina;%>
        <%
            String menu = "Reportes", ruta = "../", buscar, mensaje="", clase_="";
            boolean mostrar_msg=false;
            String persona, movimiento, total, accion;
            
            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }
            
            persona = request.getParameter("persona");
            if (persona == null) {
                persona = "";
            }
            
            movimiento = request.getParameter("movimiento");
            if (movimiento == null) {
                movimiento = "";
            }
            
            total = request.getParameter("total");
            if (total == null) {
                total = "";
            }
              
            accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
        %>
    </head>
    <body>

        <%@page import="reserva.models.Reportes" %>
        <%@page import="reserva.controllers.ReportesCtrl" %>
        <%
            ReportesCtrl rep_ctrl = new ReportesCtrl();
            
            if(accion.equals("SI")){
              mostrar_msg = true;
              Reportes rep = new Reportes();
              rep.setPersona(persona);
              rep.setMovimiento(movimiento);
              rep.setTotal(total);
            };
            
            
            
            ArrayList listado = rep_ctrl.listado(buscar);
        %>   

        <div class="d-flex" id="wrapper">

            <!-- Sidebar   sirve para crear un espacio ->&nbsp;-->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <h1 class="display-4">Reportes</h1>

                    <div class="row">
                        <div class="col-12 col-md-6">

                            <a href="index.jsp" class="btn btn-outline-success">
                                <span class="oi oi-loop-circular" title="" aria-hidden="true"></span>
                                Actualizar
                            </a>

                        </div>

                        <div class="col-12 col-md-6 ">
                            <form class="form-inline float-right" autocomplete="off" method="get" action="index.jsp">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search" name="buscar" autofocus value="<%=buscar%>">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                            </form>
                        </div>




                    </div>
                    <br>

                    <!-- TABLAAA -->
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Persona</th>
                                    <th scope="col">Movimiento</th>
                                    <th scope="col">Total</th>
                                   
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    for (int i = 0; i < listado.size(); i++) {
                                        Reportes rep = (Reportes) listado.get(i);

                                %>

                                <tr>
                                    <td width="20" ><%=i + 1%></td>
                                    <td width="70"><%=rep.getPersona()%></td>
                                    <td width="70"><%=rep.getMovimiento()%></td>
                                    <td width="70"><%=rep.getTotal()%></td>
                                    <!-- Llamar a la funcion JS para mandar el "ID" para activarse cuando click en el icono editar -->
                                    
                                </tr>
                                <% }%>

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Modal -->
        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                
                 <!--FORMULARIOOO -->
                 
                <form name="myform" method="post" action="index.jsp">
                    <input type="hidden" name="accion" value="SI">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle"> <span id="NewEdit"></span> curso </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div id="formularioNewEdit"></div>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                     </div>
              </form>      
                    
            </div>
        </div>
        
        
        <!-- Modal FOTO -->
        <div class="modal fade" id="modalFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                
                 <!--FORMULARIOOO -->
                 
                 <form name="myformfoto" method="post" action="subir.jsp" enctype="MULTIPART/FORM-DATA">
                    <input type="hidden" name="accion" value="SI">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle"> Foto de <span id="nombreCurso"></span> / Cargar / Actualizar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div id="NewEditFoto"></div>


                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                     </div>
              </form>      
                    
            </div>
        </div>
        

        
        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>
        
        
        <%
            if(mostrar_msg){
        %>
        
         <!-- Alertaaa --> 
        
        <div class="alert alert-<%=clase_%> alert_dinamic" id="alertMs" >
           <%=mensaje%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
         <% } %>
         
         <script type="text/javascript">
            function new_or_edit(id) {

                /* Si hay un par�metro imprimira editar, si no hay nada, imrprimira Nuevo */

                if (id) {
                    $('#NewEdit').html("Editar");
                } else {
                    $('#NewEdit').html("Nuevo");
                }

                $("#modalForm").modal({show: true});
                $('#formularioNewEdit').html("Cargando...");

                /*Aqui se llama al jsp form, donde estar� el formulario, mostr�ndolo aqu�*/
                $.ajax({
                    type: 'GET',
                    url: 'form.jsp',
                    data: {'id': id},
                    success: function (data) {
                        $('#formularioNewEdit').html(data);
                    }
                });
            }
          
         function new_foto(id, nombre){
             
             /* Si hay un par�metro imprimira editar, si no hay nada, imrprimira Nuevo */

                $("#modalFoto").modal({show: true});
                $("#nombreCurso").html(nombre);
                $('#NewEditFoto').html("Cargando...");

                /*Aqui se llama al jsp form, donde estar� el formulario, mostr�ndolo aqu�*/
                $.ajax({
                    type: 'GET',
                    url: 'foto.jsp',
                    data: {'id': id},
                    success: function (data) {
                        $('#NewEditFoto').html(data);
                    }
                });
         }
         
         
         
       
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
            
            window.setTimeout(function(){
                $('#alertMs').alert('close')
            }, 3000);
            
        </script>

    </body>
</html>