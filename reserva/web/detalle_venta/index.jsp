<%-- 
    Document   : index
    Created on : 19-feb-2019, 17:32:30
    Author     : Cleiser
--%>


<%@page import="reserva.models.Detalle_venta"%>
<%@page import="reserva.controllers.Detalle_ventaCtrl"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title>DETALLES DE LAS VENTAS</title>
        <%! String pagina;%>
        <%
            String menu = "detalle_venta", ruta = "../", buscar, mensaje = "", clase_ = "";
            boolean mostrar_msg = false;
            String id, producto_id, venta_id, precio, cantidad, accion;

            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }
            id = request.getParameter("id");
            if (id == null) {
                id = "";
            }
            producto_id = request.getParameter("producto_id");
            if (producto_id == null) {
                producto_id = "";
            }
            venta_id = request.getParameter("venta_id");
            if (venta_id == null) {
                venta_id = "";
            }
            precio = request.getParameter("precio");
            if (precio == null) {
                precio = "";
            }
            cantidad = request.getParameter("cantidad");
            if (cantidad == null) {
                cantidad = "";
            }
            accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
        %>
    </head>
    <body>
        <%@page import="reserva.models.Detalle_venta" %>
        <%@page import="reserva.controllers.Detalle_ventaCtrl"%>

        <%
            Detalle_ventaCtrl dv_ctrl = new Detalle_ventaCtrl();

            if (accion.equals("SI")) {
                mostrar_msg = true;
                Detalle_venta dv = new Detalle_venta();
                dv.setProducto_id(producto_id);
                dv.setVenta_id(venta_id);
                dv.setPrecio(precio);
                dv.setCantidad(cantidad);

                if (id.equals("")) {
                    if (dv_ctrl.add(dv)) {
                        clase_ = "success";
                        mensaje = "<b>Correcto!!!</b> el curso se registro correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> el curso no se registro correctamente";
                    }

                } else {
                    dv.setId(id);
                    if (dv_ctrl.update(dv)) {
                        clase_ = "success";
                        mensaje = "<b>Correcto!!!</b> el curso se autualizo correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> el curso no se actualizo correctamente";
                    }

                }

            }
            if (accion.equals("DEL")) {
                if (dv_ctrl.delete(id)) {
                    clase_ = "succes";
                    mensaje = "<b>Correcto</b> el curso se elimin� correctamente";
                } else {
                    clase_ = "danger";
                    mensaje = "<b>Error!</b> no se pudo eliminar curso";
                }
            }
            ArrayList listado = dv_ctrl.listado(buscar);
        %>



        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <h1 class="display-4">DETALLES</h1>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-12">                             

                                </div>
                                <div class="col-12">
                                    <blockquote class="blockquote">

                                    </blockquote>
                                </div>
                                <div class="col-12">
                                    <a href="../producto/index.jsp" class="btn btn-outline-dark"><span class="oi oi-chevron-left" aria-hidden="true"></span> Volver</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="row">
                                <div class=" col text-right">
                                    <a href="index.jsp" class="btn btn-outline-success"><span class="oi oi-loop-circular" title="" aria-hidden="true"></span> Actualizar</a>
                                    <button onclick="javascript:new_or_edit('');" type="button" class="btn btn-outline-primary"><span class="oi oi-plus" title="" aria-hidden="true"></span>A�adir</button>
                                </div>
                            </div>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-sm table-hover">
                                    <thead>

                                        <tr>                                     
                                            <th scope="col">N�</th>
                                            <th scope="col">Producto</th>
                                            <th scope="col">Venta</th>
                                            <th scope="col">Precio</th> 
                                            <th scope="col">Cantidad</th>
                                            <th  scope="col" colspan="2" class="text-center">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            for (int i = 0; i < listado.size(); i++) {
                                                Detalle_venta dv = (Detalle_venta) listado.get(i);
                                        %>
                                        <tr>
                                            <th width="20"><%=i + 1%></th>
                                            <td width="70"><%=dv.getProducto_id()%></td>
                                            <td><%=dv.getVenta_id()%></td>
                                            <td><%=dv.getPrecio()%></td>
                                            <td><%=dv.getCantidad()%></td>

                                            <td width="60" class="text-center" title="Editar"><a href="javascript:new_or_edit('<%=dv.getId()%>')"><span class="oi oi-pencil" title="Editar" aria-hidden="true" ></span></a></td>
                                            <td width="60" class="text-center" title="Borrar"><a href="index.jsp?accion=DEL&id=<%=dv.getId()%>"><span class="oi oi-trash" title="Eliminar" aria-hidden="true"></span></a></td>
                                            </td>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
                <br>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


    </div>

    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <form name="myform" method="post" action="index.jsp">
                <input type="hidden" name="accion" value="SI"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle"><span id="NewEdit"></span>Producto <span id='abreviaturaCap'></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="FormularioNewEdit"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" >Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /#wrapper -->

    <script src="../recursos/js/jquery-3.3.1.min.js"></script>
    <script src="../recursos/js/bootstrap.bundle.js"></script>
    <script src="../recursos/js/bootstrap.js"></script>

    <%
        if (mostrar_msg) {
    %>
    <div class="alert alert-<%=clase_%> alert_dinamic" id="alertMs" role="alert">
        <%=mensaje%>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <%}%>
    <script type="text/javascript">
                                        function new_or_edit(id) {

                                            if (id) {
                                                $('#NewEdit').html("Editar");
                                            } else {
                                                $('#NewEdit').html("Nuevo");
                                            }
                                            $("#modalForm").modal({show: true});

                                            $('#FormularioNewEdit').html("Cargando...");

                                            $.ajax({
                                                type: 'GET',
                                                url: 'form.jsp',
                                                data: {'id': id},
                                                success: function (data) {
                                                    $('#FormularioNewEdit').html(data);
                                                }
                                            });
                                        }
                                        //$('.alert').alert();


                                        $("#menu-toggle").click(function (e) {
                                            e.preventDefault();
                                            $("#wrapper").toggleClass("toggled");
                                        });
                                        window.setTimeout(function () {
                                            $('#alertMs').alert('close');
                                        }, 3000);
    </script>

</body>
</html>
