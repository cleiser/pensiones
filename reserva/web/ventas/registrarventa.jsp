<%-- 
    Document   : registrarventa
    Created on : 28-feb-2019, 3:58:46
    Author     : Cleiser
--%>

<%@page import="reserva.controllers.ProductoCtrl"%>
<%@page import="reserva.models.Detalle_venta"%>
<%@page import="reserva.models.Venta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%
    String id;
    String opcion;
    String precio_producto;
    String datos_producto;
    boolean mostrar_msg = false;
    String tipo_comprobante_id, cliente_id, vendedor_id, correlativo, fecha, total_venta;
    String producto_id, precio_venta, cantidad;

    id = request.getParameter("id");
    opcion = request.getParameter("opcion");
    if (opcion == null) {
        opcion = "";
    }

    if (id == null) {
        id = "";
    }

    tipo_comprobante_id = request.getParameter("tipo_comprobante_id");
    if (tipo_comprobante_id == null) {
        tipo_comprobante_id = "";
    }
    cliente_id = request.getParameter("cliente_id");
    if (cliente_id == null) {
        cliente_id = "";
    }
    vendedor_id = request.getParameter("vendedor_id");
    if (vendedor_id == null) {
        vendedor_id = "";
    }
    correlativo = request.getParameter("correlativo");
    if (correlativo == null) {
        correlativo = "";
    }
    fecha = request.getParameter("fecha");
    if (fecha == null) {
        fecha = "";
    }
    total_venta = request.getParameter("total_venta");
    if (total_venta == null) {
        total_venta = "";
    }

    producto_id = id;

    cantidad = request.getParameter("cantidad");
    if (cantidad == null) {
        cantidad = "";
    }
    precio_venta = request.getParameter("precio_venta");
    if (precio_venta == null) {
        precio_venta = "";
    }

    mostrar_msg = true;

    if (opcion.equalsIgnoreCase("registrar_venta")) {
        if (id != null) {
            Venta vent = new Venta();
            vent.setTipo_comprobante_id(tipo_comprobante_id);
            vent.setCliente_id(cliente_id);
            vent.setVendedor_id(vendedor_id);
            vent.setCorrelativo(correlativo);
            vent.setFecha(fecha);
            vent.setTotal(total_venta);

            Detalle_venta det_vent = new Detalle_venta();
            det_vent.setProducto_id(producto_id);
            det_vent.setPrecio(precio_venta);
            det_vent.setCantidad(cantidad);

            ProductoCtrl pro_ctrl = new ProductoCtrl();
            String id_det_vent= pro_ctrl.registrarVenta(vent, det_vent);
            out.println(id_det_vent);            
        }
    }


%>

