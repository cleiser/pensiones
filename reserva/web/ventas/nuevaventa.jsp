<%-- 
    Document   : nuevaventa
    Created on : 26-feb-2019, 20:05:18
    Author     : Cleiser
--%>

<%@page import="reserva.models.Persona"%>
<%@page import="reserva.controllers.PersonaCtrl"%>
<%@page import="reserva.models.Tipo_comprobante"%>
<%@page import="java.util.ArrayList" %>
<%@page import="reserva.controllers.Tipo_comprobanteCtrl"%>


<%@page import="reserva.models.Producto" %>
<%@page import="reserva.controllers.ProductoCtrl" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title>NUEVA VENTA</title>
        <style>
            #btnAgregarVenta, #btnAdd ,#imprimir{
                margin-top: 30px;
            }
            
        </style>
        <%! String pagina;%>
        <%! String menu = "Producto", ruta = "../", buscar, mensaje = "", clase_ = "";
        boolean mostrar_msg = false;
        %>
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>
                <%
                    Tipo_comprobanteCtrl tc_ctrl = new Tipo_comprobanteCtrl();
                    ArrayList tipo_comprobante = tc_ctrl.listado("");

                    ProductoCtrl pro_ctrl = new ProductoCtrl();
                    ArrayList producto = pro_ctrl.listado("");

                    PersonaCtrl per_ctrl = new PersonaCtrl();
                    ArrayList persona = per_ctrl.listado("");

                %>
                <div class="container-fluid">
                    <div class="table-responsive">
                        <h3 class="display-4 text-center" id="tlventa">Registrar Venta</h3>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="vendedor_id">Vendedor</label>
                                <select name="vendedor_id" required type="number" class="form-control" id="vendedor_id">
                                    <%                                        for (int i = 0; i < persona.size(); i++) {

                                            Persona per = (Persona) persona.get(i);
                                    %>
                                    <option value="<%=per.getId()%>"><%=per.getNombres()%></option>
                                    <%}%>
                                </select>
                            </div>

                            <div class="col-12 col-md-4"">
                                <label for="fecha">Fecha</label>
                                <input type="date" class="form-control" id="fecha" placeholder="" name="fecha" maxlength="60" required autofocus autocomplete="off" min="1">
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="cliente_id">Cliente</label>
                                <select name="cliente_id" required type="number" class="form-control" id="cliente_id">
                                    <%
                                        for (int i = 0; i < persona.size(); i++) {
                                            Persona per = (Persona) persona.get(i);
                                    %>
                                    <option value="<%=per.getId()%>"><%=per.getNombres()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="tipo_comprobante_id" >Comprobante</label>
                                <select name="tipo_comprobante_id" required type="text" class="form-control" id="tipo_comprobante_id">
                                    <%
                                        for (int i = 0; i < tipo_comprobante.size(); i++) {
                                            Tipo_comprobante tc = (Tipo_comprobante) tipo_comprobante.get(i);
                                    %>
                                    <option value="<%=tc.getId()%>"><%=tc.getNombre()%></option>
                                    <%}%>

                                </select>
                            </div>
                            <div class="col-12 col-md-3">
                                <label for="correlativo">Correlativo</label>
                                <input type="number" class="form-control" id="correlativo" placeholder="" name="correlativo" maxlength="10" required autofocus autocomplete="off">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <label for="tipo_producto">Producto:</label>
                                <select name="producto_id" required type="text" class="form-control" id="producto_id">

                                    <%
                                        for (int i = 0; i < producto.size(); i++) {
                                            Producto pro = (Producto) producto.get(i);
                                    %>
                                    <option value="<%=pro.getId()%>"><%=pro.getNombre()%></option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-12 col-md-2">                                
                                <label for="precio">Precio </label>                          
                                <input type="text" class="form-control" id="precio" placeholder="" name="precio" maxlength="10" required autofocus autocomplete="off" min="1">
                            </div>
                            <div class="col-12 col-md-2">
                                <label for="cantidad">Cantidad</label>
                                <input type="number" class="form-control" id="cantidad" placeholder="" name="cantidad" maxlength="60" required autofocus autocomplete="off" min="1">
                            </div>
                            <div class="col-12 col-md-2">
                                <button onclick="javascript:add();" id="btnAdd" type="button" class="btn btn-outline-success"><span class="oi oi-plus" title="" aria-hidden="true"></span> Añadir</button>
                            </div>

                        </div>

                        <br>
                        <table class="table table-bordered table-sm table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>                                 
                                    <th scope="col">Producto</th>
                                    <th scope="col">Precio</th>                                   
                                    <th scope="col">Cant.</th>                           
                                    <th scope="col">Subtotal</th>
                                    <th scope="col" colspan="2" class="text-center ">Opcion</th>
                                </tr>
                            </thead>
                            <tbody id="mis_productos">

                            </tbody>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <button onclick="javascript:addVenta();" id="btnAgregarVenta" type="button" class="btn btn-outline-success"><span class="oi oi-plus" title="" aria-hidden="true"></span> Registrar Venta</button>
                        </div>
                        <div class="col-lg-3">

                        </div>
                        <div class="col-lg-3">
                            <h2 style="margin-top: 30px;">Total: <span id="text_total_venta"></span> </h2>
                            <input type="hidden" name="total_venta" id="total_venta">
                        </div>
                        
                        <div class="col-lg-3">
                            <button onclick="" id="imprimir" type="button" class="btn btn-outline-danger"><span class="oi oi-list-rich" title="" aria-hidden="true"></span>Imprimir</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>

        <!-- /#wrapper -->

        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>
        
        <%
            if (mostrar_msg) {
        %>

        <div  class="alert alert-<%=clase_%> alert-dismissible fade alert_dinamic show" id="alertMs">
            <%=mensaje%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <%}%>
        <script type="text/javascript">
                                $(document).on('change', '#producto_id', function () {
                                    getPrecio($("#producto_id").val());

                                });
                                
                                function getPrecio(id) {
                                    $.ajax({
                                        type: 'POST',
                                        url: 'funciones.jsp',
                                        data: {'id': id, 'opcion': 'precio'},
                                        success: function (data) {
                                            $('#precio').val(data);
                                        }
                                    });
                                }

                                var arreglo = [];
                                var fila = "";
                                var lista = "";
                                var subtotal = 0;
                                var total_venta = 0;
                                function add() {
                                    var cantidad = $("#cantidad").val();
                                    var id = $("#producto_id").val();
                                    $.ajax({
                                        type: 'POST',
                                        url: 'funciones.jsp',
                                        data: {'id': id, 'opcion': 'producto'},
                                        success: function (data) {
                                            /*capturo data que la variable String de java para dividirla y formar un array javascript*/
                                            //console.log(data);
                                            /* divido la variable java con el método split() para separar los datos de java. 
                                             * Guardo los datos en la variable "res", que se convertiria en un array */
                                            var res = data.split("/");                                      
                                            /* agrego los datos en forma de objetos en un arreglo*/
                                            arreglo.push({"id": res[0], "nombre": res[1], "precio_venta": res[2], "cantidad": cantidad});
    
                                            for (var i = 0; i < arreglo.length; i++) {

                                                subtotal = arreglo[i]["cantidad"] * arreglo[i]["precio_venta"];
                                                fila = '<tr class="fila">' +
                                                        '<td class="#">' + arreglo[i]["id"] + '</td>' +
                                                        '<td class="#">' + arreglo[i]["nombre"] + '</td>' +
                                                        '<td class="#">' + arreglo[i]["precio_venta"] + '</td>' +
                                                        '<td class="#">' + arreglo[i]["cantidad"] + '</td>' +
                                                        '<td class="subtotal">' + subtotal + '</td>' +
                                                        '<td width="60"class="text-center">' +
                                                        '<a href=""><span class="oi oi-trash " title="Borrar" aria-hidden="true"></span></a>' +
                                                        '</td>' +
                                                        '</tr>';


                                            }
                                            total_venta = total_venta + subtotal;
                                            lista = lista + fila;
                                            $('#mis_productos').html(lista);

                                            $('#total_venta').val(0);
                                            $('#total_venta').val(total_venta);
                                            $('#text_total_venta').html("");
                                            $('#text_total_venta').html(total_venta);

                                        }
                                    });

                                }

                                function addVenta() {

                                    var tipo_comprobante_id = $("#tipo_comprobante_id").val();
                                    var cliente_id = $("#cliente_id").val();
                                    var vendedor_id = $("#vendedor_id").val();
                                    var correlativo = $("#correlativo").val();
                                    var fecha = $("#fecha").val();
                                    var total_venta = $("#total_venta").val();

                                    console.log("tipo_comprobante_id: " + tipo_comprobante_id);
                                    console.log("cliente_id: " + cliente_id);
                                    console.log("vendedor_id: " + vendedor_id);
                                    console.log("correlativo: " + correlativo);
                                    console.log("fecha: " + fecha);
                                    console.log("total_venta: " + total_venta);


                                    for (var i = 0; i < arreglo.length; i++) {
                                        var id = arreglo[i]["id"];
                                        var precio_venta = arreglo[i]["precio_venta"];
                                        var cantidad = arreglo[i]["cantidad"];
                                        console.log("id " + id);
                                        console.log("precio_venta " + precio_venta);
                                        console.log("cantidad " + cantidad);
                                        $.ajax({
                                            type: 'POST',
                                            url: 'registrarventa.jsp',
                                            data: {'id': id, 
                                                'opcion': 'registrar_venta', 
                                                'tipo_comprobante_id': tipo_comprobante_id,
                                                'cliente_id': cliente_id,
                                                'vendedor_id': vendedor_id,
                                                'correlativo': correlativo,
                                                'fecha': fecha,
                                                'total_venta': total_venta,
                                                'precio_venta': precio_venta,
                                                'cantidad': cantidad},
                                            success: function (data) {
                                                /*capturo data que la variable String de java para dividirla y formar un array javascript*/
                                                console.log(data);
                                                alert("Venta se registro correctamente");
                                            },
                                            error: function (data) {
                                                alert(" La Venta no  se registro correctamente");
                                            }
                                        });

                                    }
                                }
        </script>
    </body>
</html>


