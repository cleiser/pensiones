<%-- 
    Document   : index
    Created on : 19-feb-2019, 17:32:30
    Author     : Cleiser
--%>


<%@page import="reserva.models.Persona"%>
<%@page import="reserva.models.Venta"%>
<%@page import="reserva.controllers.VentaCtrl"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title>VENTAS</title>
        <%! String pagina;%>
        <%
            String menu = "Producto", ruta = "../", buscar, mensaje = "", clase_ = "";
            boolean mostrar_msg = false;
            String id,tipo_comprobante_id,correlativo,cliente_id,fecha,total,vendedor_id,estado,accion;
            
            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }
            id = request.getParameter("id");
            if (id == null) {
                id = "";
            }
            tipo_comprobante_id = request.getParameter("tipo_comprobante_id");
            if (tipo_comprobante_id == null) {
                tipo_comprobante_id = "";
            }
            correlativo = request.getParameter("correlativo");
            if (correlativo == null) {
                correlativo = "";
            }
            cliente_id = request.getParameter("cliente_id");
            if (cliente_id == null) {
                cliente_id = "";
            }
            fecha = request.getParameter("fecha");
            if (fecha == null) {
                fecha = "";
            }
            total = request.getParameter("total");
            if (total == null) {
                total = "";
            }
            vendedor_id = request.getParameter("vendedor_id");
            if (vendedor_id == null) {
                vendedor_id = "";
            }
            estado = request.getParameter("estado");
            if (estado == null) {
                estado = "";
            }
            accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
           
        %>
    </head>
    <body>
       
        <%@page import="reserva.models.Producto" %>
        <%@page import="reserva.controllers.ProductoCtrl"%>
        <%
            VentaCtrl ven_ctrl = new VentaCtrl();
            
            if (accion.equals("SI")) {
                mostrar_msg = true;
                Venta ven = new Venta();
                ven.setTipo_comprobante_id(tipo_comprobante_id);
                ven.setCorrelativo(correlativo);
                ven.setCliente_id(cliente_id);
                ven.setFecha(fecha);
                ven.setTotal(total);
                ven.setVendedor_id(vendedor_id);
                ven.setFecha(fecha);
                ven.setEstado(estado);
                
               // Persona per = new Persona();
               // per.setNombres(nombres);
                if (id.equals("")) {
                    if (ven_ctrl.add(ven)) {
                        clase_="success";
                        mensaje="<b>Correcto!!!</b> el curso se registro correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> el curso no se registro correctamente";
                    }

                } else {
                    ven.setId(id);
                    if (ven_ctrl.update(ven)) {
                        clase_ = "success";
                        mensaje = "<b>Correcto!!!</b> el curso se autualizo correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> el curso no se actualizo correctamente";
                    }

                }

            }
            if(accion.equals("DEL")){
                if (ven_ctrl.delete(id)) {
                        clase_="succes";
                        mensaje="<b>Correcto</b> el curso se elimin� correctamente";
                    } else {
                        clase_="danger";
                        mensaje="<b>Error!</b> no se pudo eliminar curso";
                    }
            }
            ArrayList listado = ven_ctrl.listado(buscar);
        %>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <div class="table-responsive">
                        <h1 class="display-4">Mis Ventas</h1>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="index.jsp" class="btn btn-outline-success"><span class="oi oi-loop-circular" title="" aria-hidden="true"></span> Actualizar</a>
                                <a href="nuevaventa.jsp" class="btn btn-outline-primary"><span class="oi oi-plus" title="" aria-hidden="true"></span> Nuevo</a>
                            </div>

                            <div class="col-12 col-md-6" >
                                <form class="form-inline float-right" autocomplete="off" method="get" action="index.jsp" >
                                    <input class="form-control mr-sm-4" type="search" placeholder="Search" 
                                           aria-label="Search" name="buscar" autofocus value="<%=buscar%>">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                                </form>
                            </div>

                        </div>
                        <br>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#">Ant</a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">4</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Sig</a>
                                </li>
                            </ul>
                        </nav>
                        <br>
                        <table class="table table-bordered table-sm table-hover">
                            <thead>

                                <tr>
                                    <th scope="col">N�</th>                                 
                                    <th scope="col">Comprobante</th>
                                    <th scope="col">Correlativo</th>                                   
                                    <th scope="col">Cliente</th>                           
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Vendedor</th>                                 
                                    <th scope="col">Estado</th>
                                    <th scope="col" colspan="2" class="text-center ">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < listado.size(); i++) {
                                        Venta ven = (Venta) listado.get(i);
                                %>
                                <tr>
                                    <td><%=i + 1%></td>
                                    <td><%=ven.getTipo_comprobante_id()%>
                                    
                                    </td>
                                    <td><%=ven.getCorrelativo()%></td>
                                    <td><%=ven.getCliente_id()%></td>
                                    <td><%=ven.getFecha()%></td>
                                    <td><%=ven.getTotal()%></td>
                                    <td><%=ven.getVendedor_id()%></td>
                                  
                                    <td>
                                        <span class="badge badge-success">Activo</span>
                                    </td>
                                    <td width="60" class="text-center"><a href="javascript:new_or_edit('<%=ven.getId()%>')"><span class="oi oi-pencil " title="Editar" aria-hidden="true"></span></a></td>
                                    <td width="60" class="text-center"><a href="index.jsp?accion=DEL&id=<%=ven.getId()%>"><span class="oi oi-trash " title="Borrar"   aria-hidden="true"></span></a></td>
                                </tr>
                                <%}%>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Modal -->

        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <form name="myform" method="post" action="index.jsp">
            <div class="modal-dialog modal-lg" role="document">
                
                    <input type="hidden" name="accion" value="SI"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle"><span id="NewEdit"></span> Venta<span id='abreviaturaCap'></span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="FormularioNewEdit"></div>
                        </div>
                        <div class="modal-footer">
                             <button type="submit" class="btn btn-danger" >Imprimir</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                    </div>
               
            </div>
                 </form>
        </div>
        
        
        
        
       
        <!-- /#wrapper -->

        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>

        <%
            if (mostrar_msg) {
        %>

        <div  class="alert alert-<%=clase_%> alert-dismissible fade alert_dinamic show" id="alertMs">
            <%=mensaje%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <%}%>
        
     

        <script type="text/javascript">
            function new_or_edit(id) {

                if (id) {
                    $('#NewEdit').html("Editar");
                } else {
                    $('#NewEdit').html("Nuevo");
                }
                $("#modalForm").modal({show: true});

                $('#FormularioNewEdit').html("Cargando...");
                //url: 'form.jsp',
                $.ajax({
                    type: 'GET',
                    url: 'form.jsp',
                    data: {'id': id},
                    success: function (data) {
                        $('#FormularioNewEdit').html(data);
                    }
                });
            }
            //$('.alert').alert();

           
   
 
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
            window.setTimeout(function () {
                $('#alertMs').alert('close');
            }, 3000);
        </script>
        

    </body>
</html>
