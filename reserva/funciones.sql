CREATE OR REPLACE FUNCTION public.registrar_venta(
	_tipo_comprobante_id integer,
	_cliente_id integer,
	_vendedor_id integer,
	_correlativo character varying,
	_fecha character varying,
	_total numeric(12,2),
    _producto_id integer,
    _venta_id integer,
    _precio numeric(12,2),
    _cantidad numeric)

    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

 declare
 _id integer;
begin 
 --- Aqui capturo el id para venta 
 select nextval('seq_id_venta') into _id;
 --- Aqui inserto la venta
INSERT INTO public.venta(id, tipo_comprobante_id, cliente_id, vendedor_id, correlativo, fecha, total, estado)
	VALUES (_id, _tipo_comprobante_id, _cliente_id, _vendedor_id, _correlativo, now(), _total, '1');

 ---Aqui inserto el usuario
INSERT INTO public.detalle_venta(producto_id, venta_id, precio, cantidad)
								VALUES (_producto_id, _id, _precio, _cantidad);
 
 return _id;
end;
 
$BODY$;

LANGUAGE plpgsql;

/***********************************************/

SELECT public.registrar_venta(
	1, 
	1, 
	4, 
	'003', 
	'hola', 
	500.00, 
	3, 
	23.00, 
	3
)