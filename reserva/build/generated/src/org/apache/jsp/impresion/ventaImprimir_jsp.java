package org.apache.jsp.impresion;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.awt.Image;
import javax.swing.GroupLayout.Alignment;
import java.io.DataOutputStream;
import com.itextpdf.text.Document;
import java.io.ByteArrayOutputStream;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Paragraph;
import java.io.DataOutput;
import java.io.*;
import java.awt.Color;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.BaseColor;
import java.util.Calendar;
import java.util.Vector;
import java.sql.ResultSet;
import reserva.controllers.Conexion;

public final class ventaImprimir_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        ");

        HttpSession sesionOk = request.getSession();
        Vector codigo = (Vector) sesionOk.getAttribute("codigo");
        Vector canti = (Vector) sesionOk.getAttribute("canti");
        Vector val = (Vector) sesionOk.getAttribute("val");
        Vector idProducto = (Vector) sesionOk.getAttribute("idProducto");
        String nomCli = (String)sesionOk.getAttribute("sesionNomCliente");
        String idCli = (String)sesionOk.getAttribute("sesionIdCliente");
        String gerente = (String)sesionOk.getAttribute("gerente");
        
      out.write('\n');
      out.write('\n');

response.setContentType ( "application/pdf");
Document document = new Document();
ByteArrayOutputStream buffer = new ByteArrayOutputStream ();
PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
document.open (); // de aqui para abajo se forma el documento
Image imagen = Image.getInstance ("/img1.jpg");//este es para la imagen del logo
Paragraph titulo = new Paragraph("Factura de venta a Clientes");//titulo
Paragraph espacio = new Paragraph("                     ");//espacio
titulo.setAlignment(Element.ALIGN_CENTER);//para centrar el titulo
PdfPTable table=new PdfPTable(5);
PdfPCell cell = new PdfPCell (new Paragraph ("Factura"));
cell.setColspan (5);
cell.setHorizontalAlignment (Element.ALIGN_CENTER);
cell.setPadding (10.0f);
table.addCell (cell);
table.addCell("Cliente");
table.addCell(nomCli);
table.addCell("");
table.addCell("Id");
table.addCell(idCli);
table.addCell("Vendedor");
table.addCell("");
table.addCell(gerente);
table.addCell("");
table.addCell("");
table.addCell("Numero");
table.addCell("Nombre");
table.addCell("Valor");
table.addCell("Cantidad");
table.addCell("Valor Parcial");
    double total = 0;
    double totalIva = 0;
    for (int i = 0; i < codigo.size(); i++)
    {
        Object miCodigo = codigo.elementAt(i);
        Object miValor = val.elementAt(i);
        Object miCan = canti.elementAt(i);
        long vpar = 0;
        vpar = Long.parseLong(val.elementAt(i).toString()) * Long.parseLong(canti.elementAt(i).toString());
        table.addCell(""+(i+1));
        table.addCell(""+miCodigo);
        table.addCell(""+miValor);
        table.addCell(""+miCan);
        table.addCell(""+vpar);
        total += vpar;
    }
    totalIva = total * 0.16;
table.addCell("Total Iva");
table.addCell("" + totalIva);
table.addCell("");
table.addCell("Total Factura");
table.addCell("" + total + totalIva);
//de aqui para abajo agregamos lo que queremos
document.add(imagen);
document.add(titulo);
document.add(espacio);
document.add(table);
document.close ();
DataOutput output = new DataOutputStream (response.getOutputStream ());
byte [] bytes = buffer.toByteArray ();
response.setContentLength (bytes.length);
for(int i = 0; i <bytes.length; i ++)
{
    output.writeByte (bytes [i]);
}

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
