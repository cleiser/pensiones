<%@page import="reserva.controllers.PersonaCtrl"%>
<%@page import="reserva.models.Persona"%>
<%@page import="reserva.models.Venta"%>
<%@page import="reserva.controllers.VentaCtrl"%>
<%@page import="java.util.ArrayList" %>
<%@page import="reserva.models.Tipo_comprobante"%>
<%@page import="reserva.controllers.Tipo_comprobanteCtrl"%>

<%@page import="reserva.models.Detalle_venta"%>
<%@page import="reserva.controllers.Detalle_ventaCtrl"%>
<%@page import="reserva.models.Producto" %>
<%@page import="reserva.controllers.ProductoCtrl" %>


<div class="container">
    <%
        String id;
        id = request.getParameter("id");
    %>
    <input type="hidden" name="id" value="<%=id%>"/>
    <%
        Tipo_comprobanteCtrl tc_ctrl = new Tipo_comprobanteCtrl();
        ArrayList tipo_comprobante = tc_ctrl.listado(id);

        ProductoCtrl pro_ctrl = new ProductoCtrl();
        ArrayList producto = pro_ctrl.listado(id);
    %>

    <a href="../detalle_venta/index.jsp">Detalles</a>
</div>

    <div class="row">

        <div class="col-6 col-sm-6 col-large-6">
            <label for="tipo_comprobante_id" >Comprobante</label>
            <select name="tipo_comprobante_id" required type="text" class="form-control" id="tipo_comprobante_id">
                <%
                    for (int i = 0; i < tipo_comprobante.size(); i++) {
                        Tipo_comprobante tc = (Tipo_comprobante) tipo_comprobante.get(i);
                %>
                <option value="<%=tc.getId()%>"><%=tc.getNombre()%></option>
                <%}%>

            </select>
        </div>

        <div class="form-group col-sm-6">
            <label for="correlativo">Correlativo</label>
            <input type="number" class="form-control" id="version" placeholder="" name="correlativo" maxlength="10" required autofocus autocomplete="off">
        </div>

    </div>

    <div class="form-row">
        <%
            PersonaCtrl per_ctrl = new PersonaCtrl();
            ArrayList persona = per_ctrl.listado(id);
        %>
        <div class="form-group col-sm-6">

            <label for="vendedor_id">Vendedor</label>
            <select name="vendedor_id" required type="number" class="form-control" id="vendedor_id">
                <%
                    for (int i = 0; i < persona.size(); i++) {
                        Persona per = (Persona) persona.get(i);
                %>
                <option value="<%=per.getId()%>"><%=per.getNombres()%></option>
                <%}%>
            </select>  

        </div>
        <div class="form-group col-sm-6">
            <label for="cliente_id">Cliente</label>
            <select name="cliente_id" required type="number" class="form-control" id="cliente_id">
                <%
                    for (int i = 0; i < persona.size(); i++) {
                        Persona per = (Persona) persona.get(i);
                %>
                <option value="<%=per.getId()%>"><%=per.getNombres()%></option>
                <%}%>
            </select>  

        </div>

        <div class="form-group col-sm-6">
            <label for="fecha">Fecha</label>
            <input type="date" class="form-control" id="leccion" placeholder="" name="fecha" maxlength="60" required autofocus autocomplete="off" min="1">
        </div>
    </div>

    <div class="row">
        <div class="col-6 col-sm-5 col-large-5">
            <label for="tipo_producto">Producto:</label>
            <select name="producto_id" required type="text" class="form-control" id="producto_id">

                <%
                    for (int i = 0; i < producto.size(); i++) {
                        Producto pro = (Producto) producto.get(i);
                %>
                <option value="<%=pro.getId()%>"><%=pro.getNombre()%></option>
                <%}%>

            </select>                               
        </div>
        <div class="form-group col-sm-2">
            <label for="precio">Precio </label>
            <input type="number" class="form-control" id="precio" placeholder="" name="precio" maxlength="10" required autofocus autocomplete="off" min="1">
        </div>
        <div class="form-group col-sm-2">
            <label for="cantidad">Cantidad</label>
            <input type="number" class="form-control" id="cantidad" placeholder="" name="cantidad" maxlength="60" required autofocus autocomplete="off" min="1">
        </div>
        <div class="form-group col-sm-2">
            <button>Agregar</button>
        </div>
    </div>


    <!-- Table -->
    <div class="container-fluid">
        <div class="table-responsive">
            <table class="table table-bordered table-sm table-hover">
                <thead>

                    <tr>
                        <th scope="col">N�</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Cant.</th>
                        <th scope="col">Subtotal</th>
                        <th scope="col" colspan="2" class="text-center ">Opciones</th>
                    </tr>
                </thead>
                <tbody>                 
                    <tr class="producto">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td width="60" class="text-center"><a href="javascript:edit('')"><span class="oi oi-pencil " title="Editar" aria-hidden="true"></span></a></td>
                        <td width="60"class="text-center"><a href="index.jsp?accion=DEL&id="><span class="oi oi-trash " title="Borrar"   aria-hidden="true"></span></a></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td width="60" class="text-center"><a href="javascript:edit('')"><span class="oi oi-pencil " title="Editar" aria-hidden="true"></span></a></td>
                        <td width="60"class="text-center"><a href="index.jsp?accion=DEL&id="><span class="oi oi-trash " title="Borrar"   aria-hidden="true"></span></a></td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group col-sm-6">
                <label for="total">Total</label>
                <input type="number" class="form-control" id="leccion" placeholder="" name="total" maxlength="60" required autofocus autocomplete="off" min="1">
            </div>
        </div>
    </div>
    <!-- Table --->


<%
    if (!id.equals("")) {


%>
<%@page import="reserva.models.Venta" %>
<%@page import="reserva.controllers.VentaCtrl" %>
<%        VentaCtrl ven_ctrl = new VentaCtrl();
    Venta ven = ven_ctrl.edit(id);
%>
<script>
    var form = document.myform;
    form.tipo_comprobante_id.value = '<%=ven.getTipo_comprobante_id()%>';
    form.correlativo.value = '<%=ven.getCorrelativo()%>';
    form.cliente_id.value = '<%=ven.getCliente_id()%>';
    form.fecha.value = '<%=ven.getFecha()%>';
    form.total.value = '<%=ven.getTotal()%>';
    form.vendedor_id.value = '<%=ven.getVendedor_id()%>';
    form.estado.value = '<%=ven.getEstado()%>';

</script>
<%}%>
