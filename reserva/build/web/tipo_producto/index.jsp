
<%@page import="reserva.models.Tipo_producto"%>
<%@page import="reserva.controllers.Tipo_productoCtrl"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title>PRINICIPAL | CURSOS B�BLICOS</title>
        <%! String pagina;%>
        <%
            String menu = "Tipo_producto", ruta = "../", buscar;
            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }
        %>
    </head>
    <body>
        <%@page import="reserva.models.Tipo_producto" %>
        <%@page import="reserva.controllers.Tipo_productoCtrl"%>
        <%
            Tipo_productoCtrl tp_ctrl = new Tipo_productoCtrl();
            ArrayList listado = tp_ctrl.listado(buscar);
        %>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <div class="table-responsive">
                        <h1 class="display-4">Categorias</h1>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="index.jsp" class="btn btn-outline-success"><span class="oi oi-loop-circular" title="" aria-hidden="true"></span> Actualizar</a>
                                <button type="button" class="btn btn-outline-primary"><span class="oi oi-plus" title="" aria-hidden="true"></span> Nuevo</button>
                            </div>

                            <div class="col-12 col-md-6" >
                                <form class="form-inline float-right" autocomplete="off" method="get" action="index.jsp" >
                                    <input class="form-control mr-sm-4" type="search" placeholder="Search" aria-label="Search" autofocus value="buscar">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                                </form>
                            </div>

                        </div>
                        <br>
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>                                   
                                    <th scope="col">N�</th>
                                    <th scope="col">Nombre</th>                                                                    
                                    <th scope="col">Estado</th>
                                   <th  scope="col" colspan="2" class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < listado.size(); i++) {
                                        Tipo_producto tp = (Tipo_producto) listado.get(i);
                                %>

                                <tr>
                                    <td width="10"><%=i + 1%></td>
                                    <td width="60"><%=tp.getNombre()%></td>
                                   
                                    <td width="40">
                                        <span class="badge badge-success">Activo</span>
                                    </td>
                                    <td width="100" class="text-center">
                                        <button type="button" class="btn btn-warning btn-sm text-center" data-toggle="modal" data-target="#modalNuevo">
                                            <i class="icon-pencil"></i>
                                        </button> &nbsp;
                                        <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#modalEliminar">
                                            <i class="icon-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                <%}%>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

    </body>
</html>
