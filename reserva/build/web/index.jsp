
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="icon" href="recursos/img/img1.png" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="recursos/css/bootstrap.css" media="all">
        <link href="recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="recursos/css/simple-sidebar.css" rel="stylesheet">
        <link href="recursos/css/styles2.css" rel="stylesheet">
        <link href="recursos/css/bootstrap.colorpickersliders.css" rel="stylesheet" type="text/css"/>
        <title>PRINICIPAL | PENSITEC</title>
        <%! String pagina;%>
        <%
            String menu = "Inicio", ruta = "";

        %>
    </head>
    <style>
        body{
            background-image:  url("recursos/img/pac-man-halloween.gif");
            background-size: cover;
        }
        
        h1{
            color: #ffffff;
            font-size: 90px;
            padding-top: 100px;
            text-align: center;
        }
    </style>
    <body>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="navbar.jsp" %>

                <div class="container-fluid">
                    <h1>Sistema de registro para la Pensi�n Florcita</h1>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

            <div class="fixed-bottom"> 
            <%@include file="footer.jsp" %>

            </div>
        </div>
        <!-- /#wrapper -->

        <script src="recursos/js/jquery-3.3.1.min.js"></script>
        <script src="recursos/js/bootstrap.bundle.js"></script>
        <script src="recursos/js/bootstrap.js"></script>
        <script src="recursos/js/bootstrap.colorpickersliders.min.js" type="text/javascript"></script>
        <script src="recursos/js/bootstrap.colorpickersliders.js" type="text/javascript"></script>

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

    </body>
</html>